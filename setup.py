"""A setuptools based setup module.

See:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='pyval',  # FIXME: Unique name
    version='0.0.1',
    description='Script to evaluate python expressions',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/astronouth7303/pyval',
    author='Jamie Bliss',
    author_email='astronouth7303@gmail.com',
    classifiers=[  # Optional
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Developers',
        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3',
    ],
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),  # Required
    entry_points={  # Optional
        'console_scripts': [
            'pyval=pyval:main',
        ],
    },
    install_requires=[
        'astunparse',
    ],
)
